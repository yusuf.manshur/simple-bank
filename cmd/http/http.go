package http

import (
	"context"

	"gitlab.com/yusuf.manshur/simple-bank/internal/consts"
	"gitlab.com/yusuf.manshur/simple-bank/internal/server"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/logger"
)

// Start function handler starting http listener
func Start(ctx context.Context) {
	serve := server.NewHTTPServer()
	defer serve.Done()

	logger.Info(logger.MessageFormat("starting simple-bank services... %d", serve.Config().App.Port), logger.EventName(consts.LogEventNameServiceStarting))

	if err := serve.Run(ctx); err != nil {
		logger.Warn(logger.MessageFormat("service stopped, err: %s", err.Error()), logger.EventName(consts.LogEventNameServiceStarting))
	}
}
