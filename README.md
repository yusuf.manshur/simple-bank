# simple-bank

simple-bank

## Running the application on a docker container

This application you can run on a docker container.

### Prerequisite

* Docker
* OS Linux Docker Image,
* Golang version 1.19 or the latest

### Environment Variable available

### Build docker image

```bash
$ docker build -t {IMAGE_NAME} -f deployment/dockerfile .

# example
$ docker build -t simple-bank -f deployment/dockerfile .
```

### Run a docker container after building the image

```bash
# example run http serve:

$ docker run -i --name simple-bank -p 8081:8081 -t simple-bank
```

## Running on your local machine

Linux or MacOS

### Installation guide

#### 1. Install Go version 1.17

Please read this link installation guide of go <https://golang.org/doc/install>

#### 2. Create a directory workspace

Run the command below:

```bash
mkdir $HOME/go
mkdir $HOME/go/src
mkdir $HOME/go/pkg
mkdir $HOME/go/bin
mkdir -p $HOME/go/src/gitlab.com/yusuf.manshur/simple-bank
chmod -R 775 $HOME/go
cd $HOME/go/src/gitlab.com/yusuf.manshur/simple-bank
export GOPATH=$HOME/go
```

Edit bash profile in $HOME/.bash_profile and
add below to new line in file .bash_profile

```bash
PATH=$PATH:$HOME/bin:$HOME/go/bin
export PATH
export GOPATH=$HOME/go
```

Run command:

```bash
source $HOME/.bash_profile
```

#### 3. Build the application

Run command:

```bash
cd $HOME/go/src/gitlab.com/yusuf.manshur/simple-bank
git clone -b development https://gitlab.com/yusuf.manshur/simple-bank .
cd $HOME/go/src/gitlab.com/yusuf.manshur/simple-bank
go mod tidy && go mod download && go mod vendor
cp config/app.yaml.dist config/app.yaml
```

Edit config/app.yaml with the server environment the build the app

```bash
go build
```

Run application after build or create on *supervisord*

```bash
./simple-bank http
```

#### 4. Health check route path

```http
GET {{host}}/liveness
```

### Run a docker-compose on your local machine

```bash
docker-compose -f deployment/docker-compose.yaml --project-directory . up -d --build
```

## Postman Collection

```go
```

## Database Migration

Migration up

```bash
go run main.go db:migrate up
```

Migration down

```bash
go run main.go db:migrate down
```

Migration reset

```bash
go run main.go db:migrate reset
```

Migration redo

```bash
go run main.go db:migrate redo
```

Migration status

```bash
go run main.go db:migrate status
```

Create migration table

```bash
go run main.go db:migrate create {tableName} sql

# example
go run main.go db:migrate create users sql
```

To show all command

```bash
go run main.go db:migrate
```

## Generate automatic CRUD

### Generate entity, presentation, repositories, use case, and dto

Make sure already have a database and table

```bash
go run main.go gen:all {tableName}

# example
go run main.go gen:all users
```

### Generate entity only

Make sure already have a database and table

```bash
go run main.go gen:entity {tableName}

# example
go run main.go gen:entity users
```

### Generate use case only

No needed database

```bash
go run main.go gen:logic {packageName} {fileName}

# example
go run main.go gen:logic users user_create
```

### Generate presentation only

Make sure already have a database and table

```bash
go run main.go gen:io {tableName}

# example
go run main.go gen:io users
```
