package auth

import (
	"context"
	"net/http"
	"time"
)

type ApiKey struct {
	AuthAppURL   string
	ApiKeyHeader string
	ApiKey       string
}

// NewApiKey create new instance of ApiKey service
func NewApiKey(AuthAppURL, apiKeyHeader, apiKey string) Service {
	return &ApiKey{AuthAppURL: AuthAppURL, ApiKeyHeader: apiKeyHeader, ApiKey: apiKey}
}

func (a *ApiKey) Validate(_ context.Context, accessKey string) (int, error) {
	req, err := http.NewRequest(http.MethodGet, a.AuthAppURL, nil)
	if err != nil {
		return http.StatusServiceUnavailable, err
	}
	req.Header.Set(a.ApiKeyHeader, accessKey)

	client := http.Client{
		Timeout: 5 * time.Second,
	}

	res, err := client.Do(req)
	if err != nil {
		return http.StatusServiceUnavailable, err
	}

	return res.StatusCode, nil
}
