package auth

import "context"

type Service interface {
	Validate(context context.Context, accessToken string) (int, error)
}
