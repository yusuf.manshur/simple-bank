package router

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/yusuf.manshur/simple-bank/internal/handler"
	"gitlab.com/yusuf.manshur/simple-bank/internal/repositories"
	"gitlab.com/yusuf.manshur/simple-bank/internal/ucase/accounts"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/databasex"
)

func (rtr *router) RouterAccount(route *mux.Router, db databasex.Adapter) {
	repoAccount := repositories.NewAccount(db)

	route.HandleFunc("/accounts", rtr.handle(
		handler.HttpRequest,
		accounts.NewAccountList(repoAccount),
	)).Methods(http.MethodGet)

	route.HandleFunc("/accounts", rtr.handle(
		handler.HttpRequest,
		accounts.NewAccountCreate(repoAccount),
	)).Methods(http.MethodPost)

	route.HandleFunc("/accounts/{id:[0-9]+}", rtr.handle(
		handler.HttpRequest,
		accounts.NewAccountUpdate(repoAccount),
	)).Methods(http.MethodPut, http.MethodPatch)

	route.HandleFunc("/accounts/{id:[0-9]+}", rtr.handle(
		handler.HttpRequest,
		accounts.NewAccountDelete(repoAccount),
	)).Methods(http.MethodDelete)
}
