// Package entity
// Automatic generated
package entity

import (
	"time"
)

// Account entity
type Account struct{
	ID int64	`db:"id,omitempty" json:"id"`
	Owner string	`db:"owner,omitempty" json:"owner"`
	Balance int64	`db:"balance,omitempty" json:"balance"`
	CreatedAt time.Time	`db:"created_at,omitempty" json:"created_at"`
	UpdatedAt time.Time	`db:"updated_at,omitempty" json:"updated_at"`
	DeletedAt *time.Time	`db:"deleted_at,omitempty" json:"deleted_at"`
}
