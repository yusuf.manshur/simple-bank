// Package accounts
// Automatic generated
package accounts

import (
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"

	"gitlab.com/yusuf.manshur/simple-bank/internal/appctx"
	"gitlab.com/yusuf.manshur/simple-bank/internal/consts"
	"gitlab.com/yusuf.manshur/simple-bank/internal/presentations"
	"gitlab.com/yusuf.manshur/simple-bank/internal/repositories"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/logger"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/tracer"

	ucase "gitlab.com/yusuf.manshur/simple-bank/internal/ucase/contract"
)

type accountCreate struct {
	repo repositories.Accounter
}

// NewAccountCreate new instance
func NewAccountCreate(repo repositories.Accounter) ucase.UseCase {
	return &accountCreate{repo: repo}
}

// Serve create Account data
func (u *accountCreate) Serve(dctx *appctx.Data) appctx.Response {
	var (
		param presentations.AccountParam
		ctx   = tracer.SpanStart(dctx.Request.Context(), "ucase.account_create")
		lf    = logger.NewFields(
			logger.EventName("AccountCreate"),
		)
	)
	defer tracer.SpanFinish(ctx)

	err := dctx.Cast(&param)
	if err != nil {
		logger.WarnWithContext(ctx, fmt.Sprintf("error parsing request params: %v", err), lf...)
		return *appctx.NewResponse().WithMsgKey(consts.RespValidationError)
	}

	err = validation.ValidateStruct(&param,
		validation.Field(&param.Owner, validation.Required),
	)
	if err != nil {
		logger.WarnWithContext(ctx, fmt.Sprintf("validation error %v", err), lf...)
		return *appctx.NewResponse().WithMsgKey(consts.RespValidationError).WithError(err)
	}

	af, err := u.repo.Store(ctx, param)
	if err != nil {
		tracer.SpanError(ctx, err)
		logger.ErrorWithContext(ctx, fmt.Sprintf("store data to database error: %v", err), lf...)
		return *appctx.NewResponse().WithMsgKey(consts.RespError)
	}

	lf.Append(logger.Any("affected_rows", af))
	logger.InfoWithContext(ctx, "success store data to database", lf...)

	return *appctx.NewResponse().
		WithMsgKey(consts.RespSuccess)
}
