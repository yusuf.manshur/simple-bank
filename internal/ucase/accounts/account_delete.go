// Package accounts
// Automatic generated
package accounts

import (
	"errors"
	"fmt"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/yusuf.manshur/simple-bank/internal/appctx"
	"gitlab.com/yusuf.manshur/simple-bank/internal/consts"
	"gitlab.com/yusuf.manshur/simple-bank/internal/presentations"
	"gitlab.com/yusuf.manshur/simple-bank/internal/repositories"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/logger"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/tracer"

	ucase "gitlab.com/yusuf.manshur/simple-bank/internal/ucase/contract"
)

type accountDelete struct {
	repo repositories.Accounter
}

// NewAccountDelete new instance
func NewAccountDelete(repo repositories.Accounter) ucase.UseCase {
	return &accountDelete{repo: repo}
}

// Serve delete Account data
func (u *accountDelete) Serve(dctx *appctx.Data) appctx.Response {
	var (
		param presentations.AccountQuery
		ctx   = tracer.SpanStart(dctx.Request.Context(), "ucase.account_delete")
		lf    = logger.NewFields(
			logger.EventName("AccountDelete"),
		)
	)
	defer tracer.SpanFinish(ctx)

	params := mux.Vars(dctx.Request)
	accountID, err := strconv.Atoi(params["id"])
	if err != nil {
		logger.WarnWithContext(ctx, fmt.Sprintf("error parsing query url: %v", err), lf...)
		return *appctx.NewResponse().WithMsgKey(consts.RespValidationError)
	}

	param.ID = int64(accountID)

	af, err := u.repo.Delete(ctx, param)
	if err != nil {
		tracer.SpanError(ctx, err)
		logger.ErrorWithContext(ctx, fmt.Sprintf("delete data from database error: %v", err), lf...)
		return *appctx.NewResponse().WithMsgKey(consts.RespError)
	}
	if af == 0 {
		err = errors.New("account not found")
		logger.WarnWithContext(ctx, fmt.Sprintf("failed delete data from database: %v", err), lf...)
		return *appctx.NewResponse().WithCode(consts.CodeUnprocessableEntity).WithMessage("Account not found")
	}

	lf.Append(logger.Any("affected_rows", af))
	logger.InfoWithContext(ctx, "success delete data from database", lf...)

	return *appctx.NewResponse().
		WithMsgKey(consts.RespSuccess)
}
