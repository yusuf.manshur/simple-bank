// Package accounts
// Automatic generated
package accounts

import (
	"errors"
	"fmt"
	"strconv"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/gorilla/mux"

	"gitlab.com/yusuf.manshur/simple-bank/internal/appctx"
	"gitlab.com/yusuf.manshur/simple-bank/internal/consts"
	"gitlab.com/yusuf.manshur/simple-bank/internal/presentations"
	"gitlab.com/yusuf.manshur/simple-bank/internal/repositories"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/logger"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/tracer"

	ucase "gitlab.com/yusuf.manshur/simple-bank/internal/ucase/contract"
)

type accountUpdate struct {
	repo repositories.Accounter
}

// NewAccountUpdate new instance
func NewAccountUpdate(repo repositories.Accounter) ucase.UseCase {
	return &accountUpdate{repo: repo}
}

// Serve update Account data
func (u *accountUpdate) Serve(dctx *appctx.Data) appctx.Response {
	var (
		input presentations.AccountUpdateParam
		where presentations.AccountQuery
		ctx   = tracer.SpanStart(dctx.Request.Context(), "ucase.account_update")
		lf    = logger.NewFields(
			logger.EventName("AccountUpdate"),
		)
	)
	defer tracer.SpanFinish(ctx)

	err := dctx.Cast(&input)
	if err != nil {
		logger.WarnWithContext(ctx, fmt.Sprintf("error parsing request params: %v", err), lf...)
		return *appctx.NewResponse().WithMsgKey(consts.RespValidationError)
	}

	err = validation.ValidateStruct(&input,
		validation.Field(&input.Balance, validation.Required, validation.Min(int64(1))),
	)
	if err != nil {
		logger.WarnWithContext(ctx, fmt.Sprintf("validation error %v", err), lf...)
		return *appctx.NewResponse().WithMsgKey(consts.RespValidationError).WithError(err)
	}

	params := mux.Vars(dctx.Request)
	accountID, err := strconv.Atoi(params["id"])
	if err != nil {
		logger.WarnWithContext(ctx, fmt.Sprintf("error parsing query url: %v", err), lf...)
		return *appctx.NewResponse().WithMsgKey(consts.RespValidationError)
	}

	where.ID = int64(accountID)

	af, err := u.repo.Update(ctx, input, where)
	if err != nil {
		tracer.SpanError(ctx, err)
		logger.ErrorWithContext(ctx, fmt.Sprintf("update data from database error: %v", err), lf...)
		return *appctx.NewResponse().WithMsgKey(consts.RespError)
	}
	if af == 0 {
		err = errors.New("account not found")
		logger.WarnWithContext(ctx, fmt.Sprintf("failed update data from database: %v", err), lf...)
		return *appctx.NewResponse().WithCode(consts.CodeUnprocessableEntity).WithMessage("Account not found")
	}

	lf.Append(logger.Any("affected_rows", af))
	logger.InfoWithContext(ctx, "success update data from database", lf...)

	return *appctx.NewResponse().
		WithMsgKey(consts.RespSuccess)
}
