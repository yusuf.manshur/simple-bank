// Package accounts
// Automatic generated
package accounts

import (
	"fmt"

	validation "github.com/go-ozzo/ozzo-validation/v4"

	"gitlab.com/yusuf.manshur/simple-bank/internal/appctx"
	"gitlab.com/yusuf.manshur/simple-bank/internal/common"
	"gitlab.com/yusuf.manshur/simple-bank/internal/consts"
	"gitlab.com/yusuf.manshur/simple-bank/internal/dto"
	"gitlab.com/yusuf.manshur/simple-bank/internal/presentations"
	"gitlab.com/yusuf.manshur/simple-bank/internal/repositories"
	"gitlab.com/yusuf.manshur/simple-bank/internal/validator"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/logger"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/tracer"

	ucase "gitlab.com/yusuf.manshur/simple-bank/internal/ucase/contract"
)

type accountList struct {
	repo repositories.Accounter
}

// NewAccountList new instance
func NewAccountList(repo repositories.Accounter) ucase.UseCase {
	return &accountList{repo: repo}
}

// Serve Account list data
func (u *accountList) Serve(dctx *appctx.Data) appctx.Response {
	var (
		param presentations.AccountQuery
		ctx   = tracer.SpanStart(dctx.Request.Context(), "ucase.account_list")
		lf    = logger.NewFields(
			logger.EventName("AccountList"),
		)
	)
	defer tracer.SpanFinish(ctx)

	err := dctx.Cast(&param)
	if err != nil {
		logger.WarnWithContext(ctx, fmt.Sprintf("error parsing query url: %v", err), lf...)
		return *appctx.NewResponse().WithMsgKey(consts.RespValidationError)
	}

	err = validation.ValidateStruct(&param,
		validation.Field(&param.Page, validation.Min(int64(1))),
		validation.Field(&param.Limit, validation.Min(int64(1))),
		validation.Field(&param.StartDate, validator.ValidDateTime()),
		validation.Field(&param.EndDate, validator.ValidDateTime()),
	)
	if err != nil {
		logger.WarnWithContext(ctx, fmt.Sprintf("validation error %v", err), lf...)
		return *appctx.NewResponse().WithMsgKey(consts.RespValidationError).WithError(err)
	}

	param.Limit = common.LimitDefaultValue(param.Limit)
	param.Page = common.PageDefaultValue(param.Page)

	dr, count, err := u.repo.FindWithCount(ctx, param)
	if err != nil {
		tracer.SpanError(ctx, err)
		logger.ErrorWithContext(ctx, fmt.Sprintf("error find data from database: %v", err), lf...)
		return *appctx.NewResponse().WithMsgKey(consts.RespError)
	}

	logger.InfoWithContext(ctx, fmt.Sprint("success fetch accounts from database"), lf...)

	return *appctx.NewResponse().
		WithMsgKey(consts.RespSuccess).
		WithData(dto.AccountsToResponse(dr)).
		WithMeta(appctx.MetaData{
			Page:       param.Page,
			Limit:      param.Limit,
			TotalCount: count,
			TotalPage:  common.PageCalculate(count, param.Limit),
		})
}
