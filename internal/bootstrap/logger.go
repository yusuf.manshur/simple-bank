// Package bootstrap
package bootstrap

import (
	"gitlab.com/yusuf.manshur/simple-bank/internal/appctx"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/logger"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/util"
)

func RegistryLogger(cfg *appctx.Config) {
	logger.Setup(logger.Config{
		Environment: util.EnvironmentTransform(cfg.App.Env),
		Debug:       cfg.App.Debug,
		Level:       cfg.Logger.Level,
		ServiceName: cfg.Logger.Name,
	})
}

