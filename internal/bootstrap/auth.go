package bootstrap

import (
	"gitlab.com/yusuf.manshur/simple-bank/internal/appctx"
	"gitlab.com/yusuf.manshur/simple-bank/internal/auth"
)

// RegistryAuthApiKey initialize API Key auth service
func RegistryAuthApiKey(cfg *appctx.Config) auth.Service {
	return auth.NewApiKey(cfg.App.AuthAppURL, cfg.App.ApiKeyHeader, cfg.App.ApiKey)
}
