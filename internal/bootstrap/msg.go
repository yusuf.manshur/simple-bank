// Package bootstrap
package bootstrap

import (
	"gitlab.com/yusuf.manshur/simple-bank/internal/consts"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/logger"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/msgx"
)

func RegistryMessage()  {
	err := msgx.Setup("msg.yaml", consts.ConfigPath)
	if err != nil {
		logger.Fatal(logger.MessageFormat("file message multi language load error %s", err.Error()))
	}

}
