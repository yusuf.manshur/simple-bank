package dto

import (
	"gitlab.com/yusuf.manshur/simple-bank/internal/consts"
	"gitlab.com/yusuf.manshur/simple-bank/internal/entity"
	"gitlab.com/yusuf.manshur/simple-bank/internal/presentations"
)

func AccountToResponse(src entity.Account) presentations.AccountDetail {
	x := presentations.AccountDetail{
		ID:      src.ID,
		Owner:   src.Owner,
		Balance: src.Balance,
	}

	if !src.CreatedAt.IsZero() {
		x.CreatedAt = src.CreatedAt.Format(consts.LayoutDateTimeFormat)
	}

	if !src.UpdatedAt.IsZero() {
		x.UpdatedAt = src.UpdatedAt.Format(consts.LayoutDateTimeFormat)
	}

	if src.DeletedAt != nil {
		x.DeletedAt = src.DeletedAt.Format(consts.LayoutDateTimeFormat)
	}

	return x
}

func AccountsToResponse(inputs []entity.Account) []presentations.AccountDetail {
	var (
		result = []presentations.AccountDetail{}
	)

	for i := 0; i < len(inputs); i++ {
		result = append(result, AccountToResponse(inputs[i]))
	}

	return result
}
