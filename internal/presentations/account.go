// Package presentations
// Automatic generated
package presentations

type (
	// AccountQuery parameter
	AccountQuery struct {
		ID      int64  `db:"id,omitempty" json:"id" url:"id,omitempty"`
		Owner   string `db:"owner,omitempty" json:"owner" url:"owner,omitempty"`
		Balance int64  `db:"balance,omitempty" json:"balance" url:"balance,omitempty"`
		Paging
		PeriodRange
	}

	// AccountParam input param
	AccountParam struct {
		Owner   string `db:"owner,omitempty" json:"owner"`
		Balance int64  `db:"balance,omitempty" json:"balance"`
	}

	// AccountDetail detail response
	AccountDetail struct {
		ID        int64  `json:"id"`
		Owner     string `json:"owner"`
		Balance   int64  `json:"balance"`
		CreatedAt string `json:"created_at"`
		UpdatedAt string `json:"updated_at"`
		DeletedAt string `json:"deleted_at"`
	}

	// AccountUpdateParam input param
	AccountUpdateParam struct {
		Owner     string `db:"owner,omitempty" json:"owner"`
		Balance   int64  `db:"balance,omitempty" json:"balance"`
		UpdatedAt string `db:"updated_at,omitempty"`
	}
)
