// Package repositories
// Automatic generated
package repositories

import (
	"context"
	"database/sql"
	"fmt"

	"golang.org/x/sync/errgroup"

	"gitlab.com/yusuf.manshur/simple-bank/internal/common"
	"gitlab.com/yusuf.manshur/simple-bank/internal/entity"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/builderx"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/databasex"
	"gitlab.com/yusuf.manshur/simple-bank/pkg/tracer"
)

// Accounter contract of Account
type Accounter interface {
	Storer
	Updater
	Deleter
	Counter
	FindOne(ctx context.Context, param interface{}) (*entity.Account, error)
	Find(ctx context.Context, param interface{}) ([]entity.Account, error)
	FindWithCount(ctx context.Context, param interface{}) ([]entity.Account, int64, error)
}

type account struct {
	db databasex.Adapter
}

// NewAccount create new instance of Account
func NewAccount(db databasex.Adapter) Accounter {
	return &account{db: db}
}

// FindOne account
func (r *account) FindOne(ctx context.Context, param interface{}) (*entity.Account, error) {
	var (
		result entity.Account
		err    error
	)

	ctx = tracer.SpanStart(ctx, "repo.account_find_one")
	defer tracer.SpanFinish(ctx)

	qw, err := builderx.StructToPostgreQueryWhere(param, "db")
	if err != nil {
		tracer.SpanError(ctx, err)
		return nil, err
	}

	q := `
		SELECT
			id,
			owner,
			balance,
			created_at,
			updated_at,
			deleted_at
		FROM accounts %s LIMIT 1
	`

	err = r.db.QueryRow(ctx, &result, fmt.Sprintf(q, qw.Query), qw.Values...)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	return &result, err
}

// Find account
func (r *account) Find(ctx context.Context, param interface{}) ([]entity.Account, error) {
	var (
		result []entity.Account
		err    error
	)

	ctx = tracer.SpanStart(ctx, "repo.account_finds")
	defer tracer.SpanFinish(ctx)

	qw, err := builderx.StructToMySqlQueryWhere(param, "db")
	if err != nil {
		tracer.SpanError(ctx, err)
		return nil, err
	}

	q := `
		SELECT
			id,
			owner,
			balance,
			created_at,
			updated_at,
			deleted_at
		 FROM accounts %s LIMIT ? OFFSET ?
	`

	query := builderx.ToPostgrePlaceHolder(fmt.Sprintf(q, qw.Query))

	vals := qw.Values
	vals = append(vals, qw.Limit, common.PageToOffset(qw.Limit, qw.Page))

	err = r.db.Query(ctx, &result, query, vals...)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	return result, err
}

// Store account
func (r *account) Store(ctx context.Context, param interface{}) (int64, error) {
	var (
		affected int64
		err      error
	)

	ctx = tracer.SpanStart(ctx, "repo.account_store")
	defer tracer.SpanFinish(ctx)

	query, vals, err := builderx.StructToQueryInsert(param, "accounts", "db")
	if err != nil {
		tracer.SpanError(ctx, err)
		return 0, err
	}

	query = builderx.ToPostgrePlaceHolder(query)

	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	err = r.db.Transact(ctx, sql.LevelRepeatableRead, func(tx *databasex.DB) (err error) {
		affected, err = tx.Exec(ctx, query, vals...)
		return
	})

	return affected, err

}

// Update account data
func (r *account) Update(ctx context.Context, input interface{}, where interface{}) (int64, error) {
	var (
		affected int64
		err      error
	)

	ctx = tracer.SpanStart(ctx, "repo.account_update")
	defer tracer.SpanFinish(ctx)

	query, vals, err := builderx.StructToQueryUpdate(input, where, "accounts", "db")
	if err != nil {
		tracer.SpanError(ctx, err)
		return 0, err
	}

	query = builderx.ToPostgrePlaceHolder(query)

	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	err = r.db.Transact(ctx, sql.LevelRepeatableRead, func(tx *databasex.DB) (err error) {
		affected, err = tx.Exec(ctx, query, vals...)
		return
	})

	return affected, err
}

// Delete account from database
func (r *account) Delete(ctx context.Context, param interface{}) (int64, error) {
	var (
		affected int64
		err      error
	)
	ctx = tracer.SpanStart(ctx, "repo.account_delete")
	defer tracer.SpanFinish(ctx)

	query, vals, err := builderx.StructToQueryDelete(param, "accounts", "db", false)
	if err != nil {
		tracer.SpanError(ctx, err)
		return 0, err
	}

	query = builderx.ToPostgrePlaceHolder(query)

	// See https://en.wikipedia.org/wiki/Isolation_(database_systems)#Isolation_levels.
	err = r.db.Transact(ctx, sql.LevelRepeatableRead, func(tx *databasex.DB) (err error) {
		affected, err = tx.Exec(ctx, query, vals...)
		return
	})

	return affected, err
}

// Count account
func (r *account) Count(ctx context.Context, param interface{}) (total int64, err error) {
	ctx = tracer.SpanStart(ctx, "repo.account_count")
	defer tracer.SpanFinish(ctx)

	qw, err := builderx.StructToMySqlQueryWhere(param, "db")
	if err != nil {
		tracer.SpanError(ctx, err)
		return
	}

	q := `
		SELECT
        	COUNT(id) AS totoal
		FROM accounts %s
	`

	query := builderx.ToPostgrePlaceHolder(fmt.Sprintf(q, qw.Query))

	err = r.db.QueryRow(ctx, &total, query, qw.Values...)
	if err != nil {
		tracer.SpanError(ctx, err)
		return
	}

	return
}

// FindWithCount Find account with Count
func (r *account) FindWithCount(ctx context.Context, param interface{}) ([]entity.Account, int64, error) {
	var (
		cl    []entity.Account
		count int64
	)

	ctx = tracer.SpanStart(ctx, "repo.account_with_count")
	defer tracer.SpanFinish(ctx)

	group, newCtx := errgroup.WithContext(ctx)

	group.Go(func() error {
		l, err := r.Find(newCtx, param)
		cl = l
		return err
	})
	group.Go(func() error {
		c, err := r.Count(ctx, param)
		count = c
		return err
	})

	err := group.Wait()

	return cl, count, err
}
