-- +goose Up

-- +goose StatementBegin

CREATE TABLE
    IF NOT EXISTS "accounts" (
        "id" bigserial PRIMARY KEY NOT NULL,
        "owner" varchar(100) NOT NULL,
        "balance" bigint NOT NULL DEFAULT 0,
        "created_at" timestamptz NOT NULL DEFAULT (now()),
        "updated_at" timestamptz NOT NULL DEFAULT '0001-01-01 00:00:00Z',
        "deleted_at" timestamptz
    );

-- +goose StatementEnd

-- +goose Down

-- +goose StatementBegin

DROP TABLE IF EXISTS accounts;

-- +goose StatementEnd